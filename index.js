const express = require('express')
const app = express()
const port = process.env.PORT || 3001;
const MongoClient = require('mongodb').MongoClient;
const collection_name = 'marketing';

app.get('/', (req, res) => res.send('Hello World!'));

app.get('/api', async (req, res) => {
    console.log('api called');
    try {
        const db = await new MongoClient.connect(process.env.MONGODB_URI);
        const collection = db.collection(collection_name);
        const result = await collection.find().toArray();
        console.log('returning collection',result);
        res.json(result);
    }
    catch(err){
        console.log(err);
    }
});
app.listen(port, () => console.log(`Example app listening on port ${port}!`))
